package com.example.helpfqas.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.helpfqas.FAQSItem
import com.example.helpfqas.R

class MyQuestionFAQsAdapter :
    RecyclerView.Adapter<MyQuestionFAQsAdapter.MyQuestionFAQsViewHolder>() {

    private var mListItem = ArrayList<FAQSItem>()

    fun setDataForList(list: ArrayList<FAQSItem>) {
        mListItem.clear()
        mListItem.addAll(list.distinct())
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyQuestionFAQsViewHolder {
        return MyQuestionFAQsViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_my_question, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyQuestionFAQsViewHolder, position: Int) {
        val item = mListItem[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int = mListItem.size

    inner class MyQuestionFAQsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: FAQSItem) {}
        }
    }