package com.example.helpfqas.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.helpfqas.FAQSItem
import com.example.helpfqas.R

class FAQsQuestionAdapter(private val context: Context)
    : RecyclerView.Adapter<FAQsQuestionAdapter.FAQsViewHolder>() {

    private val mListData = ArrayList<FAQSItem>()

    fun setData(list: List<FAQSItem>) {
        mListData.clear()
        mListData.addAll(list)
    }

    fun clearData() {
        mListData.clear()
    }

     inner class FAQsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
         fun bind(
             listData: List<FAQSItem>,
             position: Int,
             adapter: FAQsQuestionAdapter
        ) {}
     }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FAQsViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_faqs, parent, false)
        return FAQsViewHolder(view)
    }

    override fun getItemCount(): Int = mListData.size

    override fun onBindViewHolder(holder: FAQsViewHolder, position: Int) {
        holder.bind(mListData, position, this)
    }
}